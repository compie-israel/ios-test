## iOS Programming Task

In order to be considered for the iOS position, you must complete the following steps. 

*Note: This task should take no longer than one hour at the most.*


## Task

1. Fork this repository.
2. Create a new iOS app called "My Meals". The app lets the user track his meals, add description to them, rate them, and show them all together in a single Table View. See the assets folder that contains the app screen designs.
	- "Your Meals" screen is the main screen of the app. This is the screen that is shown when the app is first started. It contains a Table View that shows the existing meals. Each meal has an image, a title and a rating. Before the user adds his first meal, the list is obviously empty.
	- Clicking the "+" button opens a new screen that allows the user to add a new meal to the list. This screen contains 3 components: (1) An editable text field of the meal title, (2) A photo of the meal. Here the app allows the user to choose a photo from his phone or take new one. (3) The meal's rating. See the two relevant designs in the assets folder: "Add a new meal empty screen", "Add a new meal filled screen".
	- If you click the edit button, the table view goes into editing mode. See "Your meals screen edit mode" design in the assets folder. You can choose a cell to delete by clicking the indicator on the left, and confirm that you want to delete it by pressing the Delete button in that cell. Alternatively, swipe left on a cell to expose the Delete button quickly.
3. Commit and Push your code to your new repository
4. Send us a pull request, we will review your code and get back to you

## Judging

Code quality: testability, encapsulation, design patterns, comments, readable code, clean code, etc.

*Note: Cheating or copying code will lead to disqualifying*